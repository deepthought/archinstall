# Deepthought's Arch install

Following is my method for installing arch with SDDM / KDE plasma in mid-2016 on an M2 drive with whole-drive encryption. The bulk of my troubleshooting time was spent with GRUB configuration on getting the machine to boot with encrypted volumes on the M2 and also unlocking the multiple volumes with a single password login. While the arch wiki is great I had to do a lot of troubleshooting that wasn't straightforward looking at the wiki material and with simple web searching. This guide is to try and help others (and myself in the future) draw the rest of the fucking owl with similar setups. Please let me know if you have any comments, recommendations, or corrections for this document. Please see/start-with the beginner's guide at https://wiki.archlinux.org/index.php/beginners'_guide

![draw an owl](http://i.imgur.com/RadSf.jpg "Draw an owl")  
image source: http://i.imgur.com/RadSf.jpg

## hardware
My hardware setup:

* CPU: Intel Core i7-6700K 4.0GHz Quad-Core Processor
* motherboard: Asus SABERTOOTH Z170 S ATX LGA1151 Motherboard
* drives:
 * Samsung 950 PRO 256GB M.2-2280 Solid State Drive
 * Samsung 850 EVO-Series 500GB 2.5" Solid State Drive
* GPU: Gigabyte GeForce GTX 970 Overclocked GDDR5 Pcie Video Graphics Card, 4GB 
* RAM: G.Skill TridentZ Series 32GB (2 x 16GB) DDR4-3000 Memory
* wireless adapter: Asus PCE-AC55BT PCI-Express x1 802.11a/b/g/n/ac Wi-Fi Adapter

sidenote: I found http://pcpartpicker.com useful in planning my build

## setup installation usb
load arch image on usb stick using dd, with notification alert for when this is done (if installing from linux system):  

    sudo dd bs=4M if=<image to flash> of=/dev/<device to flash to>; notify-send 'flash complete' 'sdimage copy has completed'

if you are loading an image from a Mac/OSX system syntax is as follows (OSX doesn't like uppercase "M" in byte size and has different alert/notification method):

    sudo dd bs=4m if=<image to flash> of=/dev/<device to flash to>; terminal-notifier -title "Terminal: dd cmd" -message "Done with image flash" -sound default -activate com.apple.Terminal
    
if you're using windows or another system there are many guides online how to flash system images to USBs.

## setup partitions and encryption 

### use gdisk to create new partition scheme for m2 drive:
resources:  
https://wiki.archlinux.org/index.php/partitioning
https://wiki.archlinux.org/index.php/GNU_Parted

list devices to identify system for installation: `lsblk`

launch gdisk (parted is a pain)  
select device (note device id may be different for you): `/dev/nvme0n1`  
make sure if you have any data on the drive that you back it up before wiping it!  
delete any existing partitions: `d`

**create new 1st main boot EFI partition of 260 Mi (will be /boot/efi)**    
start new partition: `n`  
start with default 1st partition by hitting return  
first sector at default 2048 by hitting return  
enter size of 260 MiB via: `+260M`  
enter in file system type for EFI: `ef00`

**create new 2nd encrypted boot partition (will be /boot)**  
start new partition: `n`  
default 2nd partition by return  
first sector at end of 1st by return  
size of partition 200MiB via: `+200M`  
default partition filetype 8300 by return  

**create new 3rd main encrypted LVM partition (will be root “/”)**  
start new partition: `n`  
default 3rd partition by return  
first sector at end of 2nd by return  
size of partition taking up the rest of disk via hitting return  
LVM partition type: `8E00`

write these partitions to disk: `w`  
confirm writing to disk

### setup encrypted volumes
setup LVM for 3rd partition via: `cryptsetup luksFormat /dev/nvme0n1p3`  
enter passphrase  
open the container via: `cryptsetup open --type luks /dev/nvme0n1p3 lvm`  
create LVM volume group: `vgcreate xVol /dev/mapper/lvm`  
create a logical volume taking up the entire space of the LVM: `lvcreate -l 100%FREE xVol -n root`  
format the logical volume: `mkfs.ext4 /dev/mapper/xVol-root`  
mount the LVM container: `mount dev/mapper/xVol-root /mnt`

setup encrypted boot volume: `cryptsetup luksFormat /dev/nvme0n1p2`  
enter passphrase  
open encrypted boot volume: `cryptsetup open /dev/nvme0n1p2 cryptboot`  

create filesystem for /boot: `mkfs.ext2 /dev/mapper/cryptboot`  
create /mnt/boot: `mkdir /mnt/boot`  
mount partition to /mnt/boot: `mount /dev/mapper/cryptboot /mnt/boot`  

format EFI 1st partition as fat32: `mkfs.fat -F32 /dev/nvme0n1p1`  
create mountpoint for EFI partition at /boot/efi & mount:

    mkdir /mnt/boot/efi    
    mount /dev/nvme0n1p1 /mnt/boot/efi    

**sidenote if need to reboot pre being able to boot into system, to open/mount volumes:**  
&nbsp; &nbsp; &nbsp; `cryptsetup open --type luks /dev/nvme0n1p3 lvm`  
&nbsp; &nbsp; &nbsp; `mount /dev/mapper/xVol-root /mnt`  
&nbsp; &nbsp; &nbsp; (then decrypt & mount p2, mount p1)

## install base system
execute:

    pacstrap -i /mnt base base-devel
    genfstab -U /mnt >> /mnt/etc/fstab
check the resulting file to ensure that it has root, boot, and boot/efi (if latter two not there, may have mounting in wrong order and need to remount latter two in order as per above)  
####  setup this partition to be automatically unlocked at boot via a keyfile:  
generate random keyfile: `dd bs=512 count=4 if=/dev/urandom of=/mnt/crypto_keyfile.bin iflag=fullblock`  
add key to LUKS partition header: `cryptsetup luksAddKey /dev/nvme0n1p2 /mnt/crypto_keyfile.bin`  
enter password for LUKS volume to complete this  
ALSO added keyfile to primary root partition nvme0n1p3  
edit /etc/crypttab to include:  

    cryptboot	/dev/nvme0n1p2	/crypto_keyfile.bin	luks

## basic system configuration pre 1st boot
`arch-chroot /mnt /bin/bash`

open /etc/locale.gen and uncomment “LANG=en_US.UTF-8”  
generate locales via: `locale-gen`  
add `LANG=en_US.UTF-8` in /etc/locale.conf  

run `tzselect` to select timezone and then set this up as default via symlink:  
`ln -s /usr/share/zoneinfo/America/New_York /etc/localtime`

“adjust the time skew, and set the time standard to UTC”:  
`hwclock --systohc --utc `

**configure initrd image to use encryption**  
open /etc/mkinitcpio.conf  
find line starting with “HOOKS” and before “filesystems” put `encrypt lvm2 resume`  
add `ext4` to MODULES  
add `/etc/mykeyfile` to FILES  

regenerate initrd image: `mkinitcpio -p linux`

**GRUB configuration**  
to use GRUB must install grub-git (at time of this writing default GRUB does NOT support NVME + boot encryption + UEFI) via:  
&nbsp; &nbsp; &nbsp; `git clone https://aur.archlinux.org/packages/grub-git/`  
have to add the resultant pgp key to keyring for install  
to install need to create user as well to `makepkg -sri`  

next we need to configure GRUB for LUKS encrypted /boot partition & unlock the encrypted root partition at boot  
first find UUID of main root lvm volume: `lsblk -o NAME,UUID | grep n1p3`  
write down UUID to copy into subsequent steps

install grub & efibootmgr via: ``pacman -S grub efibootmgr``

generate grub config file & install to mounted ESP:  
&nbsp; &nbsp; &nbsp; add following to /etc/default/grub:  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; find this line and append following data: `GRUB_CMDLINE_LINUX="<stuff before> cryptdevice=UUID=<device-UUID>:lvm root=/dev/mapper/xVol-root <stuff after>”`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; at next line after that add: `GRUB_ENABLE_CRYPTODISK=y`

generate grub config file: `grub-mkconfig -o /boot/grub/grub.cfg`  
&nbsp; &nbsp; &nbsp; this will generate an error: “failed to connect to lvmetad” but the following page says this is symptom of chroot and can be ignored: https://wiki.archlinux.org/index.php/GRUB#Boot_partition 
 
install grub:  
&nbsp; &nbsp; &nbsp; `grub-install --recheck /dev/nvme0n1`  
&nbsp; &nbsp; &nbsp; (sidenote: following listed in some guides does NOT work: grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=grub –recheck)

following may have useful resources re configuring grub and avoiding double passwords:

* http://missinglink.xyz/security/tutorial-debianubuntu-full-disk-encryption-luks-fde-including-encrypted-boot/
* http://missinglink.xyz/security/tutorial-debianubuntu-full-disk-encryption-luks-fde-including-encrypted-boot/improved-full-disk-encryption-add-luks-key-file-initrdinitramfs/

## setup core system elements on first boot

### setup basic networking
get network info via: `ip a`  
ensure have kernel driver via `lspci -v`  
	look for what comes after kernel driver or modules  
	then `dmesg | grep [driver name]`  
change network device name by editing (or creating:) /etc/udev/rules.d/10-network.rules  
`SUBSYSTEM=="net", ACTION=="add", ATTR{address}=="ff:ee:dd:cc:bb:aa", NAME="net0"`

### ssh setup
ssh daemon - install (via pacman), start (i.e. launch now), and enable (i.e. start on boot) it:

    pacman -S openssh
    systemctl start sshd.service  
make sshd start on boot: `systemctl enable sshd.service`  
change default port by editing /etc/ssh/sshd_config  
see ports here:

* https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers
* https://wiki.archlinux.org/index.php/Secure_Shell
 
generate ssh key using newerish algorithsm: `ssh-keygen -t ed25519`

### set time:
	pacman -S ntp
	systemctl enable ntp
	system start ntp

### setup swap file:
resources:

* https://wiki.archlinux.org/index.php/Dm-crypt/Swap_encryption
* https://wiki.archlinux.org/index.php/Swap
* https://wiki.archlinux.org/index.php/GRUB
 
&nbsp; &nbsp; &nbsp; 

    fallocate -l 4GiB /swapfile
    chmod 600 /swapfile
    mkswap /swapfile
    swapon /swapfile
 
edit /etc/fstab to include swap entry: `/swapfile none swap defaults 0 0`  
configure resume parameter for swapfile:
	`filefrag -v /swapfile`  
	find first physical offset number for `swap_file_offset`, for me: `256000`  
	as per wiki may have to edit GRUB configuation (/etc/default/grub) to resume swap, e.g.:  
	`GRUB_CMDLINE_LINUX_DEFAULT="resume=/dev/sdaX quiet"`
	OR
	`GRUB_CMDLINE_LINUX="resume=UUID=uuid-of-swap-partition"`

### install desktop environment/GUI
details will depend on your specific graphics setup!  
e.g. install xorg xf86-video-intel mesa-libgl xorg-xinit
create 20-intel.conf x11 file as per https://wiki.archlinux.org/  index.php/Intel_graphics#Skylake_support  
	&nbsp; &nbsp; &nbsp; in dir: /etc/X11/xorg.conf.d/  
	&nbsp; &nbsp; &nbsp; DO NOT DISABLE DRI! ←this will fuck everything up and prevent SDDM from launching!  
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; See: https://bbs.archlinux.org/viewtopic.php?id=207705&p=3  
ensure can startx  
	further modify 20-intel.conf file per this to fix plasma5 screen  flickering:  
	&nbsp; &nbsp; &nbsp; https://bbs.archlinux.org/viewtopic.php?pid=1595276#p1595276  
note: using some graphics cord adapters can result in screen flickering and other problems  
	&nbsp; &nbsp; &nbsp; e.g. for me a displayport to hdmi adaptor cord led to massive  problems that resolved without it!  
ensure can still `startx`  
install the following apps: `sudo pacman -S sddm plasma kde-applications  `  
set sddm to have numlock on by default by editing /etc/sddm.conf & under `[General]` section setting `Numlock=on`  
	&nbsp; &nbsp; &nbsp; https://wiki.archlinux.org/index.php/Activating_Numlock_on_Bootup  

enable/start sddm:  

    sudo systemctl enable sddm
    sudo systemctl start sddm
&nbsp; &nbsp; &nbsp; [note if fails ENSURE DRI NOT DISABLED IN xorg file!!!!!!!!]

try: `startx /usr/bin/startkde`  
&nbsp; &nbsp; &nbsp; https://bbs.archlinux.org/viewtopic.php?id=209511

re trying to fix plasma post issues:
	https://forum.kde.org/viewtopic.php?f=289&t=125034

### Get wireless working
ensure `wi` & `wpa_supplicant` installed  
see if can scan for networks via: `sudo iw dev wi0 scan | less`   
if can’t may have to troubleshoot driver & ensure using right device name
  
try connecting to wpa network via the following:  
switch to root (NOT sudo)  
`wpa_supplicant -i wi0 -c <(wpa_passphrase "<ssid name>" $'<password>')`  


caveats/notes re `wpa_supplicant`:

* if any single quotes in pass escape with `\` e.g.: `that\’s what she said`
* NEED $ to have bash parse any single quotes in single quotes  
* the -B flag can be passed to wpa_supplicant in order to run it in the background  
* flags: 
 * c = path to file (in this case imported in parentheses)  
 * i = interface to listen on  
 * B = run daemon in background  
 
check if successfully connected:  `iw dev wi0 link`  
create wpa_supplicant config file:  
`wpa_passphrase MYSSID passphrase > /etc/wpa_supplicant/example.conf`  

or can use the following to deal with special characters in password:  
`wpa_passphrase "<ssid name>" $'<password>' > /etc/wpa_supplicant/mynet.conf`

setup from config file:  
	 `wpa_supplicant -i wi0 -B -c /etc/wpa_supplicant/mynet.conf`
to close connection flush ip address & gateway, then disable the interface:

	ip addr flush dev wi0
	ip route flush dev wi0
	ip link set dev wi0 down
	
or combined close connection: `ip addr flush dev wi0; ip route flush dev wi0; ip link set dev wi0 down`

on hashing/encrypting passwords:  
	&nbsp; &nbsp; &nbsp; https://bbs.archlinux.org/viewtopic.php?id=144471


## System Software Configuration
**brightness control**  
following will set brightness to 30% if using 2 HDMI monitors:
	`xrandr --output HDMI2 --brightness 0.3; xrandr --output HDMI1 --brightness 0.3`  
I created brightness script that will perform the above (will try to include on git repo)

**key signature setup**  
see https://wiki.archlinux.org/index.php/GnuPG  
ensure that the following in gpg.conf:  
	`keyserver-options auto-key-retrieve`  
otherwise may get PGP key errors when attempting pacman upgrades

**creating bash scripts**  
create ~/bin directory  
add to path via editing .bashrc in home folder and adding `export   PATH=”$HOME/bin:$PATH”`  
put bash script in new bin directory and make executable (e.g. `chmod +x <file>`)  

**printing**  
this was much easier than I anticipated it to be  
see https://wiki.archlinux.org/index.php/CUPS  
`pacman -S ghostscript gsfonts gutenprint`  
will use cups to run print daemon service, avahi to help find/map printers, and nss-mdns to help resolve printer addresses on network  
cups & avahi should already be installed – install if not  
install nss-mdns  
next start enable these services:

    sudo systemctl start org.cups.cupsd.service
    sudo systemctl start avahi-daemon.service
    sudo systemctl enable org.cups.cupsd.service
    sudo systemctl enable avahi-daemon.service
to use network printing edit the /etc/nsswitch.conf file per CUPS arch page making the hosts line as follows:  
	`hosts: files mdns_minimal [NOTFOUND=return] dns myhostname`  
ensure printer detected via usb at `lsusb` (if via usb print)  
add printer either via:  

* kde → configure printers in system settings (I did it this way)
* or via: http://localhost:631/admin (see https://superuser.com/questions/484245/getting-canon-printers-to-work-with-arch-linux )

**scanning**  
ensure sane package and drivers are installed (e.g. from printer/scanner install as per above)  
install simple scanning program: `pacman -S simple-scan`

**fonts**  
note that after installing print/scanning software, default fonts with libreoffice may be messed up. therefore reinstall default liberation serif font:  
`pacman -S ttf-liberation`  
see https://wiki.archlinux.org/index.php/fonts

### pacman software install
enable 32-bit apps by editing /etc/pacman.conf:  
  &nbsp; &nbsp; &nbsp; uncomment `[multilib]` line and the line right after it
		
make pacman include pacman icons by editing pacman conf file w ilovecandy:   
&nbsp; &nbsp; &nbsp; see https://bbs.archlinux.org/viewtopic.php?id=121699  
&nbsp; &nbsp; &nbsp; edit pacman.conf and under the Misc options just add "ILoveCandy".
		
#### useful pacman applications
admin tools:
		`sudo pacman -S screen traceroute unzip wget samba nmap dialog bc`  
general: `sudo pacman -S firefox thunderbird pidgin pidgin-otr libreoffice-fresh hunspell hunspell-en owncloud-client keepassx2 clementine`  
development applications: `sudo pacman -S qtcreator`  
virtualization w virtualbox: `sudo pacman -S virtualbox linux-headers virtualbox-guest-iso`  
3d printing: `sudo pacman -S freecad blender`

#### useful AUR applications
how to install manually:  
	&nbsp; &nbsp; &nbsp; `git clone <url for package>`  
	&nbsp; &nbsp; &nbsp; open folder containing git clone  
	&nbsp; &nbsp; &nbsp; check the PKGBUILD file for malicious code  
	&nbsp; &nbsp; &nbsp; `makepkg -sri`  
	
packages to install:  
	&nbsp; &nbsp; &nbsp; pithos qpdfview  
	
installed pacaur to help install aur dependencies (otherwise takes 2342342 years for some applications) & switched default text editor from vi to nano  
&nbsp; &nbsp; &nbsp; note: don't install pacaur if you don't know what you're doing (can pose very significant security risk problems)

#### wine applications installation
see https://wiki.archlinux.org/index.php/wine  
install via pacman: `sudo pacman -S wine winecfg`  
install winetricks via AUR

##### sketchup
create sketchup wine profile: `WINEPREFIX=~.wine-sketchup winecfg`
	or `WINEPREFIX=~/.wine-sketchup winecfg`    
set to Windows 7 via winecfg  
download mfc100u.dll from sketchy misc website (I had to google around)  
install this dll in the 64 bit folder inside the created wine directory (`~/.wine-sketchup/drive_c/windows/syswow64`)  
download sketchup windows install file and run through wine afterwards (ensuring in same terminal where set WINEPREFIX to the sketchup specific wine instance)  
if wine installer runs successfully it should just quit  
navigate to sketchup folder in the wine install (e.g. ~/.wine-sketchup/drive_c/Program Files (x86)/SketchUp/SketchUp 2016)  
open sketchup via: `wine SketchUp.exe`  

after install to set environment variable to specific wine environment:
`env WINEPREFIX=~/.wine-sketchup`

access winecfg for a specific installation directory (this might be wrong): `WINEPREFIX=~/.wine wine winecfg`

for STL export seem to need IE6 installed as per:  
https://appdb.winehq.org/objectManager.php?sClass=version&iId=33102

### specific software install/configuration
* clementine:  
	install plugin for spotify and login  
	install streaming plugins/packages: `pacman -S gst-plugins-ugly gst-libav gvfs`  
		includes library for streaming “ugly” packages:  
		“license on either the plug-ins or the supporting libraries might not be how we'd like. The code might be widely known to present patent problems.” https://askubuntu.com/questions/468875/plugins-ugly-and-bad  
		https://www.archlinux.org/packages/extra/x86_64/gst-plugins-ugly/
* slic3r: download/install binary from main website
* firefox plugins:
	tree style tab, https-everywhere, multiple tab handler

### using encrypted file containers (rather than whole disk/device encryption)
resources:

* https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_a_non-root_file_system#Loop_device
* https://www.digitalocean.com/community/tutorials/how-to-use-dm-crypt-to-create-an-encrypted-volume-on-an-ubuntu-vps

create sized file to act as storage device – with specific file size allocation  
option 1: fallocate (will not write random data)  
&nbsp; &nbsp; &nbsp; `fallocate -l <size in GB>G <file location>`  
&nbsp; &nbsp; &nbsp; &nbsp; e.g. `fallocate -l 55G ~/taco.crypt`  
option 2: write random data with dd  
&nbsp; &nbsp; &nbsp; `dd if=/dev/urandom of=~/taco.crypt bs=1M count=<size>`  
option 3: use dev/random instead of /dev/urandom otherwise same as   option 2

get a loop slot: `losetup -f`

create device node at loop slot
	`losetup /dev/loop# /home/x/taco.crypt`

format the new loop device (MAKE SURE RIGHT DEVICE OR YOU COULD DELETE FILESYSTEMS!):  
	`cryptsetup options luksFormat /dev/loop#`

open the newly created encrypted device:  
	`cryptsetup open /dev/loop# <mount name>`  
&nbsp; &nbsp; &nbsp; e.g. `cryptsetup open /dev/loop# /cryto-taco`

format the opened crypto device:  
	`mkfs.ext4 /dev/mapper/crypto-taco`

mount the formatted crypto device:  
	`mkdir /mnt/crypt-taco`  
	`mount -t ext4 /dev/mapper/crypto-taco /mnt/crypto-taco`

after creation to mount:

    losetup -f
    losetup /dev/loop0 taco.crypt
    cryptsetup open /dev/loop0 taco_crypt
    mount -t ext4 /dev/mapper/taco_crypt /mnt/taco_crypt


### backup to crypto partition with rsync
https://wiki.archlinux.org/index.php/full_system_backup_with_rsync  
assuming the crypto partition is opened as per above  

    rsync -aAXv --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found"} <backup source directory> <backup destination directory>

notable flags include:  
&nbsp; &nbsp; &nbsp; v = verbose  
&nbsp; &nbsp; &nbsp; a = archive – recursion and preserve most things  
&nbsp; &nbsp; &nbsp; x (lowercase vs. upper?) = one file system, do not cross boundaries  
&nbsp; &nbsp; &nbsp; A = preserve ACLS  

e.g.:
`rsync -aAXv --exclude={"dev/*","proc/*","sys/*","tmp/*","run/*","mnt/*","media/*","lost+found","swapfile"} . /mnt/myvol_crypt`

## troubleshooting
**cord adapter problems**  
An HDMI to displayport adapter caused significant problems for me that seem to resolve with a differnet cord (i.e. not converting interface types). These included: screen flickering, panels disappearing or moving, and inability to type in lock screen after awakening from sleep (could move mouse and drop to shell but not type in password prompt window despite seeming cursor focus). Temporary attempts at fixing flickering that seemed to reduce but not eliminate this included altering KDE compositor settings (in KDE system settings -> displays -> compositor):  
		&nbsp; &nbsp; &nbsp; changed tearing prevention from auto to “full screen repaints”  
		&nbsp; &nbsp; &nbsp; changed rendering backend from openGL to Xrender  
	
## to do
* drop bear - for remote luks volume unlocking on boot
* figure out keyboard shortcuts to move windows across desktop environments
* software for remembering podcast/youtube/etc video place across multiple machines easily - I thought gpodder would do this but I did not have luck with this.
* make brightness script more nuanced to use e.g. keyboard shortcuts for incremental adjustments like laptops do
* remote desktop solution to take over kde5 active session (i.e. desktop :0)
		
## ongoing issues

#### firefox VERY frequently crashing 

Searching the web I was able to find others with the same issue but not a solution. dode sample:

    systemd-coredump[2026]: Process 1864 (firefox) of user 1000 dumped core.
    
    Stack trace of thread 1864:
    
    #0  0x00007ff976830f5f raise (libpthread.so.0)
    #1  0x00007ff96a50e857 n/a (libxul.so)
    #2  0x00007ff96ae505c1 n/a (libxul.so)
    #3  0x00007ff976831080 __restore_rt (libpthread.so.0)
    #4  0x00007ff96a8faf8d n/a (libxul.so)
    #5  0x00007ff96ad942d3 n/a (libxul.so)
    #6  0x00007ff96ab28d16 n/a (libxul.so)
    #7  0x00007ff96ab2a344 n/a (libxul.so)
    #8  0x00007ff96ab2b250 n/a (libxul.so)
    #9  0x00007ff96ab2b656 n/a (libxul.so)
    #10 0x00007ff96ab2d752 n/a (libxul.so)
    #11 0x00007ff96944772c n/a (libxul.so)
    #12 0x00007ff968c3b98c n/a (libxul.so)
    #13 0x00007ff968c33f61 n/a (libxul.so)
    #14 0x00007ff968c361bb n/a (libxul.so)
    #15 0x00007ff968c50678 n/a (libxul.so)
    #16 0x00007ff968e5248e n/a (libxul.so)
    #17 0x00007ff968e4246e n/a (libxul.so)
    #18 0x00007ff969ee13f8 n/a (libxul.so)
    #19 0x00007ff96a4dd8cc n/a (libxul.so)
    #20 0x00007ff96a5157d0 n/a (libxul.so)
    #21 0x00007ff96a515adc n/a (libxul.so)
    #22 0x00007ff96a515d22 XRE_main (libxul.so)
    #23 0x0000562c531733d5 n/a (firefox)
    #24 0x0000562c53172a6f n/a (firefox)
    #25 0x00007ff9759fb291 __libc_start_main (libc.so.6)
    #26 0x0000562c53172b9a _start (firefox)

    Sep 08 11:50:13 <hostname> systemd-coredump[1853]: Process 1236 (firefox) of user 1000 dumped core.
    Stack trace of thread 1236:
    #0  0x00007f1f073fdf5f raise (libpthread.so.0)
    #1  0x00007f1efb10e857 n/a (libxul.so)
    #2  0x00007f1efba505c1 n/a (libxul.so)
    #3  0x00007f1f073fe080 __restore_rt (libpthread.so.0)
    #4  0x00007f1efac3bb78 n/a (libxul.so)
    #5  0x00007f1efac48e97 n/a (libxul.so)
    #6  0x00007f1efac48b20 n/a (libxul.so)
    #7  0x00007f1efac49e8d n/a (libxul.so)
    #8  0x00007f1efac49f0a n/a (libxul.so)
    #9  0x00007f1efac48c99 n/a (libxul.so)
    #10 0x00007f1efac49e8d n/a (libxul.so)
    #11 0x00007f1efac49f0a n/a (libxul.so)
    #12 0x00007f1efac48c99 n/a (libxul.so)

#### wifi with frequent drops
This was significant enough for me to use an old linksys router as a wifi bridge over a the new wifi card re reliability. I suspect I may have left more than one service managing wifi (i.e. networkmanager plus another) but it wasn't readily apparent spending a few minutes on it.

#### trouble copying terminal data into clipboard and pasting into libreoffice
For some reason going through a simple text-editor intermediary works, making me suspect the problem is with libreoffice.

#### boot wifi error 
`<hostname> kernel: iwlwifi 0000:03:00.0: Unsupported splx structure`  
I don't know yet what is up with this or if it matters.